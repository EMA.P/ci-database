# ci-database

 - create inital seed file for database (sql schema or simply seeder)
 - create js module getData.js to get data from database using filters
 - create test with snapshot testing that is getting some data from this module with differnet filters
 - run this test both locally and in ci

# run docker

start docker toolkit
```sh
# run docker services in backgroun
docker-compose up -d --force-recreate

# connect to mysql shell
docker-compose exec mysql mysql -h mysql -u test -ptest test

# run node container 
docker-compose up node
```
